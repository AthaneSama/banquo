import 'package:banquo/Bloc/add_amount_cubit.dart';
import 'package:banquo/Bloc/main_app_cubit.dart';
import 'package:banquo/Entity/Amount.dart';
import 'package:banquo/Navigation/AddAmount.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainAppView {
  static Widget getTitleContainer(
      String title, BuildContext context, String type, String sens) {
    return Container(
        height: 50,
        color: Colors.orange,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.white,
                )),
            IconButton(
                iconSize: 30,
                color: Colors.white,
                onPressed: () {
                  context.read<AddAmountCubit>().load(type, sens);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AddAmount(title: title, type: type, sens: sens)),
                  );
                },
                icon: const Icon(Icons.add))
          ],
        ));
  }

  static Widget getListItem(List<Amount> listOfElement, bool positive) {
    return Column(children: [
      ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: listOfElement.length,
        itemBuilder: (context, i) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(listOfElement[i].name,
                      style: const TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  ),
                  const Text(">",
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  ),
                  Text((listOfElement[i].amount).toString(),
                      style: TextStyle(
                          fontSize: 25,
                          color:
                              (positive ? Colors.lightGreenAccent : Colors.red),
                          fontWeight: FontWeight.bold)),
                  const Text(" €",
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                ],
              ),
              IconButton(
                  color: Colors.white,
                  onPressed: () {
                    context.read<MainAppCubit>().deleteAmount(
                        listOfElement[i].name,
                        listOfElement[i].amount,
                        listOfElement[i].type,
                        listOfElement[i].date);
                    context.read<MainAppCubit>().load();
                  },
                  icon: const Icon(Icons.delete))
            ],
          );
        },
      ),
    ]);
  }
}
