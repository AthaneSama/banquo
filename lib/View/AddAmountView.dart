import 'package:banquo/Bloc/add_amount_cubit.dart';
import 'package:banquo/Bloc/main_app_cubit.dart';
import 'package:banquo/Entity/Amount.dart';
import 'package:banquo/Navigation/AddAmount.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddAmountView {

  static Widget getText(String text){
    return Text(
      "$text :",
      style: const TextStyle(
          fontSize: 25,
          color: Colors.white,
          fontWeight: FontWeight.bold),
    );
  }

}
