import 'package:banquo/Database/DatabaseUtils.dart';
import 'package:banquo/Entity/Amount.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'main_app_state.dart';

class MainAppCubit extends Cubit<MainAppState> {
  MainAppCubit() : super(MainAppInitial());

  DatabaseUtils dbu = DatabaseUtils();

  void load() async{

    String date = DateTime.now().month < 10 ? "0${DateTime.now().month}/${DateTime.now().year}" : "${DateTime.now().month}/${DateTime.now().year}";
    double total = 0.0;
    List<Amount> listFixedReturn = await dbu.retrieveReturnFixedAmount();
    List<Amount> listPunctualReturn = await dbu.retrieveReturnPunctualAmount(date);
    List<Amount> listFixedSpent = await dbu.retrieveSpentFixedAmount();
    List<Amount> listPunctualSpent = await dbu.retrieveSpentPunctualAmount(date);
    for(var amount in listFixedReturn){
      total += amount.amount;
    }
    for(var amount in listPunctualReturn){
      total += amount.amount;
    }
    for(var amount in listFixedSpent){
      total += amount.amount;
    }
    for(var amount in listPunctualSpent){
      total += amount.amount;
    }
    emit(MainAppLoaded(listFixedReturn, listPunctualReturn, listFixedSpent, listPunctualSpent,total));
  }

  void insertAmount(String name, double amount, String type, String date){
    Amount toInsert = Amount(amount: amount, name: name, type: type, date: date);
    dbu.insertAmount(toInsert);
  }

  void deleteAmount(String name, double amount, String type, String date){
    Amount toDelete = Amount(amount: amount, name: name, type: type, date: date);
    dbu.deleteAmount(toDelete);
  }

}
