part of 'add_amount_cubit.dart';

@immutable
abstract class AddAmountState {}

class AddAmountInitial extends AddAmountState{}

class AddAmountLoading extends AddAmountState{}

class AddAmountLoaded extends AddAmountState {

  final String type;
  final String sens;
  double totalAmount;
  final double baseTotalAmount;

  AddAmountLoaded(this.type, this.sens, this.totalAmount, this.baseTotalAmount);
}