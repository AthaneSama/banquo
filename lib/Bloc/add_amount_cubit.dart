import 'package:banquo/Entity/Amount.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../Database/DatabaseUtils.dart';

part 'add_amount_state.dart';

class AddAmountCubit extends Cubit<AddAmountState> {
  AddAmountCubit() : super(AddAmountInitial());

  DatabaseUtils dbu = DatabaseUtils();

  void load(String type, String sens) async{
    emit(AddAmountLoading());
    double total = await dbu.getTotalAmount();
    emit(AddAmountLoaded(type,sens,total,total));
  }

  void insertAmount(String name, String amount){
    if(state is AddAmountLoaded){

      String date = (state as AddAmountLoaded).type == 'F' ? "" : DateTime.now().month < 10 ? "0${DateTime.now().month}/${DateTime.now().year}" : "${DateTime.now().month}/${DateTime.now().year}";
      double finalAmount = (state as AddAmountLoaded).sens =='P' ? double.parse(amount) : - double.parse(amount);

      Amount toInsert = Amount(amount: finalAmount, name: name, type: (state as AddAmountLoaded).type, date: date);
      dbu.insertAmount(toInsert);
    }
  }
}

