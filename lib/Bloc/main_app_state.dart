part of 'main_app_cubit.dart';

@immutable
abstract class MainAppState {}

class MainAppInitial extends MainAppState{}

class MainAppLoading extends MainAppState{}

class MainAppLoaded extends MainAppState {

  final List<Amount> listFixedReturn;
  final List<Amount> listPunctualReturn;
  final List<Amount> listFixedSpent;
  final List<Amount> listPunctualSpent;
  final double totalAmount;

  MainAppLoaded(this.listFixedReturn, this.listPunctualReturn, this.listFixedSpent, this.listPunctualSpent, this.totalAmount);
}