import 'package:banquo/Bloc/add_amount_cubit.dart';
import 'package:banquo/Bloc/main_app_cubit.dart';
import 'package:banquo/Navigation/MainApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => MainAppCubit()),
          BlocProvider(create: (context) => AddAmountCubit()),
        ],
        child: MaterialApp(
          title: 'Banqu\'O',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.orange),
            useMaterial3: true,
          ),
          home: const MainApp(title: 'Banqu\'O'),
        ));
  }
}
