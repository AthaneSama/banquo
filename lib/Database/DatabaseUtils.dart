import 'package:banquo/Entity/Amount.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseUtils {
  final seuil = 0;
  final punctual = 'P';
  final fixed = 'F';

  Future<Database> getDatabase() async {
    final database = openDatabase(
      join(await getDatabasesPath(), 'banquo_database.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE amount( amount DOUBLE, name VARCHAR(30), type VARCHAR(1), date TEXT, PRIMARY KEY ( amount, name, type, date))',
        );
      },
      version: 4,
    );
    return database;
  }

  void insertAmount(Amount amount) async {
    final db = await getDatabase();

    await db.insert('amount', amount.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Amount>> retrieveAllAmount() async {
    final db = await getDatabase();

    final List<Map<String, dynamic>> maps = await db.query('amount');

    return List.generate(maps.length, (i) {
      return Amount(
          amount: maps[i]['amount'],
          name: maps[i]['name'],
          type: maps[i]['type'],
          date: maps[i]['date']);
    });
  }

  Future<double> getTotalAmount() async {
    final db = await getDatabase();

    final List<Map<String, dynamic>> maps = await db.query('amount');

    final List<Amount> allAmount = List.generate(maps.length, (i) {
      return Amount(
          amount: maps[i]['amount'],
          name: maps[i]['name'],
          type: maps[i]['type'],
          date: maps[i]['date']);
    });

    double total = 0.0;
    for(var amount in allAmount){
      if( amount.date == "" || int.parse(amount.date.substring(0,2)) == DateTime.now().month) {
        total = total + amount.amount;
      }
    }
    return total;
  }

  Future<List<Amount>> retrieveReturnPunctualAmount(String date) async {
    final db = await getDatabase();

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM amount where amount>? AND type=? and date=? ",
        [seuil, punctual, date]);

    return List.generate(maps.length, (i) {
      return Amount(
          amount: maps[i]['amount'],
          name: maps[i]['name'],
          type: maps[i]['type'],
          date: maps[i]['date']);
    });
  }

  Future<List<Amount>> retrieveReturnFixedAmount() async {
    final db = await getDatabase();

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM amount where amount>? AND type=?", [seuil, fixed]);

    return List.generate(maps.length, (i) {
      return Amount(
          amount: maps[i]['amount'],
          name: maps[i]['name'],
          type: maps[i]['type'],
          date: maps[i]['date']);
    });
  }

  Future<List<Amount>> retrieveSpentPunctualAmount(String date) async {
    final db = await getDatabase();

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM amount where amount<? AND type=? and date=? ",
        [seuil, punctual, date]);

    return List.generate(maps.length, (i) {
      return Amount(
          amount: maps[i]['amount'],
          name: maps[i]['name'],
          type: maps[i]['type'],
          date: maps[i]['date']);
    });
  }

  Future<List<Amount>> retrieveSpentFixedAmount() async {
    final db = await getDatabase();

    final List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM amount where amount<? AND type=? ", [seuil, fixed]);

    return List.generate(maps.length, (i) {
      return Amount(
          amount: maps[i]['amount'],
          name: maps[i]['name'],
          type: maps[i]['type'],
          date: maps[i]['date']);
    });
  }

  Future<void> deleteAmount(Amount amount) async {
    final db = await getDatabase();

    await db.delete('amount',
        where: 'amount = ? and name = ? and type = ? and date = ? ',
        whereArgs: [amount.amount, amount.name, amount.type, amount.date]);
  }

}
