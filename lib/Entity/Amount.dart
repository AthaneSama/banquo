class Amount{

  final double amount; // La valeur du montant
  final String name; // Le nom de la valeur saisie
  final String type; // Le type de du montant (P ponctuel, F fixe)
  final String date; // La date du mois dans lequelle ca a ete saisie (null si ponctuel)

  const Amount({
                required this.amount,
                required this.name,
                required this.type,
                required this.date});

  Map<String,dynamic> toMap(){
    return{
      'amount':amount,
      'name':name,
      'type':type,
      'date':date,
    };
  }

  @override
  String toString() {
    return 'Amount{amount: $amount, name: $name, type: $type, date: $date}';
  }
}
