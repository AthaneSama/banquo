import 'package:flutter/material.dart';

class Utils{

  /// Méthode qui permet d'afficher une animation de chargement
  static Widget loading(String title) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: Colors.orange,
      ),
      body: const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            CircularProgressIndicator(),
            Text("Chargement"),
          ],
        ),
      ),
    );
  }


}