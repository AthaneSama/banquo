import 'package:banquo/Bloc/main_app_cubit.dart';
import 'package:banquo/Utils/Utils.dart';
import 'package:banquo/View/MainAppView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainApp extends StatefulWidget {
  const MainApp({required this.title});

  final String title;

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainAppCubit, MainAppState>(builder: (context, state) {
      if (state is MainAppInitial) {
        context.read<MainAppCubit>().load();
        return Utils.loading(widget.title);
      } else if (state is MainAppLoading) {
        return Utils.loading(widget.title);
      } else if (state is MainAppLoaded) {
        return Scaffold(
          backgroundColor: Colors.black54,
          appBar: AppBar(
            centerTitle: true,
            title: Text(widget.title,
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(5.0, 5.0),
                      blurRadius: 3.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                )),
            backgroundColor: Colors.orange,
          ),
          body: SafeArea(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 1),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  MainAppView.getTitleContainer("RENTRÉE FIXE :", context,"F","P"),
                  MainAppView.getListItem(state.listFixedReturn, true),
                  MainAppView.getTitleContainer("RENTRÉE PONCTUEL :", context,"P","P"),
                  MainAppView.getListItem(state.listPunctualReturn, true),
                  MainAppView.getTitleContainer("DÉPENSE FIXE :", context,"F","N"),
                  MainAppView.getListItem(state.listFixedSpent, false),
                  MainAppView.getTitleContainer("DÉPENSE PONCTUEL :", context,"P","N"),
                  MainAppView.getListItem(state.listPunctualSpent, false),
                  Container(
                    color: Colors.white,
                    height: 5,
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 1),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Total : ",
                          style: TextStyle(
                              fontSize: 25,
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                      Text(state.totalAmount.toStringAsFixed(2),
                          style: TextStyle(
                              fontSize: 25,
                              color: (state.totalAmount < 0 ? Colors.red : Colors.lightGreenAccent),
                              fontWeight: FontWeight.bold)),
                      const Text("€ ",
                          style: TextStyle(
                              fontSize: 25,
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ],
              ),
            ),
          )),
        );
      } else {
        return const Text("Erreur lors du démarrage de l'application");
      }
    });
  }
}
