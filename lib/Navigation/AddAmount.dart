import 'package:banquo/Bloc/add_amount_cubit.dart';
import 'package:banquo/Bloc/main_app_cubit.dart';
import 'package:banquo/Utils/Utils.dart';
import 'package:banquo/View/AddAmountView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AddAmount extends StatefulWidget {
  const AddAmount(
      {required this.title, required this.type, required this.sens});

  final String title;
  final String type;
  final String sens;
  @override
  State<AddAmount> createState() => _AddAmountState();
}

class _AddAmountState extends State<AddAmount> {
  TextEditingController nomController = TextEditingController();
  TextEditingController amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AddAmountCubit, AddAmountState>(
        builder: (context, state) {
      if (state is AddAmountInitial) {
        context.read<AddAmountCubit>().load(widget.type, widget.sens);
        return Utils.loading(widget.title);
      } else if (state is AddAmountLoading) {
        return Utils.loading(widget.title);
      } else if (state is AddAmountLoaded) {
        return Scaffold(
          backgroundColor: Colors.black54,
          appBar: AppBar(
            centerTitle: true,
            title: Text("NOUVELLE ${widget.title}",
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(5.0, 5.0),
                      blurRadius: 3.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                )),
            backgroundColor: Colors.orange,
          ),
          body: SafeArea(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 1),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 1),
                    ),
                    AddAmountView.getText("Nom"),
                    TextField(
                      decoration: const InputDecoration(
                          focusColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.white, width: 4)),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.orange, width: 4))),
                      controller: nomController,
                      cursorColor: Colors.white,
                      style: const TextStyle(color: Colors.white),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 1),
                    ),
                    AddAmountView.getText("Montant"),
                    TextField(
                      decoration: const InputDecoration(
                          focusColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.white, width: 4)),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.orange, width: 4))),
                      controller: amountController,
                      onChanged: (value) {
                        setState(() {
                          if (double.tryParse(value) != null) {
                            if (state.sens == 'P') {
                              state.totalAmount =
                                  state.baseTotalAmount + double.parse(value);
                            } else {
                              state.totalAmount =
                                  state.baseTotalAmount - double.parse(value);
                            }
                          } else {
                            state.totalAmount = state.baseTotalAmount;
                          }
                        });
                      },
                      cursorColor: Colors.white,
                      style: const TextStyle(color: Colors.white),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 1),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          if (double.tryParse(amountController.text) != null) {
                            context.read<AddAmountCubit>().insertAmount(
                                nomController.text, amountController.text);
                            context.read<MainAppCubit>().load();
                            Navigator.pop(context);
                          } else {
                            Fluttertoast.showToast(
                                msg: "Le montant saisie n'est pas correct",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                        },
                        child: const Text("Valider")),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("Total : ",
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                        Text(state.totalAmount.toStringAsFixed(2),
                            style: TextStyle(
                                fontSize: 25,
                                color: (state.totalAmount < 0
                                    ? Colors.red
                                    : Colors.lightGreenAccent),
                                fontWeight: FontWeight.bold)),
                        const Text("€ ",
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )),
        );
      } else {
        return const Text("Erreur lors du démarrage de l'application");
      }
    });
  }
}
